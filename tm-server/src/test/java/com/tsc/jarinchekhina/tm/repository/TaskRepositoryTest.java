package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import java.util.List;


@FixMethodOrder
public class TaskRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final User user = bootstrap.getUserService().create("auto", "auto");

    @NotNull
    private static final String userId = user.getId();

    @AfterClass
    public static void after() {
        bootstrap.getUserService().removeByLogin("auto");
    }

    @Test
    public void testAddFindAll() {
        @NotNull final Project project = bootstrap.getProjectService().create(userId, "AUTO Project");
        @NotNull final Task taskSecond = new Task();
        taskSecond.setName("AUTO 2");
        taskSecond.setProjectId(project.getId());
        bootstrap.getTaskService().add(userId, taskSecond);
        @NotNull final Task taskFirst = new Task();
        taskFirst.setName("AUTO 1");
        bootstrap.getTaskService().add(userId, taskFirst);

        @NotNull List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(2, taskList.size());

        taskList = bootstrap.getProjectTaskService().findAllTaskByProjectId(userId, project.getId());
        Assert.assertEquals(1, taskList.size());
        Assert.assertEquals("AUTO 2", taskList.get(0).getName());

        bootstrap.getTaskService().removeByName(userId, "AUTO 1");
        bootstrap.getTaskService().removeByName(userId, "AUTO 2");
        taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(0, taskList.size());
    }

    @Test
    public void testFindRemove() {
        @NotNull Task taskById = new Task();
        taskById.setName("AUTO Id");
        bootstrap.getTaskService().add(userId, taskById);
        @NotNull Task taskByIndex = new Task();
        taskByIndex.setName("AUTO Index");
        bootstrap.getTaskService().add(userId, taskByIndex);
        @NotNull Task taskByName = new Task();
        taskByName.setName("AUTO Name");
        bootstrap.getTaskService().add(userId, taskByName);

        taskById = bootstrap.getTaskService().findById(userId, taskById.getId());
        Assert.assertEquals("AUTO Id", taskById.getName());
        @NotNull List<Task> taskList = bootstrap.getTaskService().findAll(userId);
        taskByIndex = bootstrap.getTaskService().findByIndex(userId, 1);
        Assert.assertEquals(taskList.get(0).getName(), taskByIndex.getName());
        taskByName = bootstrap.getTaskService().findByName(userId, "AUTO Name");
        Assert.assertEquals("AUTO Name", taskByName.getName());

        bootstrap.getTaskService().removeById(userId, taskById.getId());
        bootstrap.getTaskService().removeByName(userId, "AUTO Name");
        bootstrap.getTaskService().removeByIndex(userId, 1);

        taskList = bootstrap.getTaskService().findAll(userId);
        Assert.assertEquals(0, taskList.size());
    }

}
