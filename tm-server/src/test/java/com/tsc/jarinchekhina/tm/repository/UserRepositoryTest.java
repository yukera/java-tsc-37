package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;


public class UserRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Test
    public void testFindRemove() {
        @NotNull final User userFirst = new User();
        userFirst.setLogin("auto");
        userFirst.setEmail("auto email");
        @Nullable String hashPassword = HashUtil.salt(bootstrap.getPropertyService(), "auto");
        userFirst.setPasswordHash(hashPassword);
        bootstrap.getUserService().add(userFirst);

        @NotNull final User userSecond = new User();
        userSecond.setLogin("auto 2nd");
        hashPassword = HashUtil.salt(bootstrap.getPropertyService(), "auto 2nd");
        userSecond.setPasswordHash(hashPassword);
        userSecond.setEmail("auto 2nd email");
        bootstrap.getUserService().add(userSecond);

        @Nullable User userRepoFirst = bootstrap.getUserService().findByLogin("auto");
        Assert.assertNotNull(userRepoFirst);
        Assert.assertEquals("auto", userRepoFirst.getLogin());
        @Nullable User userRepoSecond = bootstrap.getUserService().findByEmail("auto 2nd email");
        Assert.assertNotNull(userRepoSecond);
        Assert.assertEquals("auto 2nd", userRepoSecond.getLogin());

        bootstrap.getUserService().removeByLogin("auto");
        bootstrap.getUserService().removeByLogin("auto 2nd");
        userRepoFirst = bootstrap.getUserService().findByLogin("auto");
        Assert.assertNull(userRepoFirst);
        userRepoSecond = bootstrap.getUserService().findByEmail("auto 2nd email");
        Assert.assertNull(userRepoSecond);
    }

}
