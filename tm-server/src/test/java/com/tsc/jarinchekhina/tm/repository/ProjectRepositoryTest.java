package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import java.util.List;


@FixMethodOrder
public class ProjectRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private static final User user = bootstrap.getUserService().create("auto", "auto");

    @NotNull
    private static final String userId = user.getId();

    @AfterClass
    public static void after() {
        bootstrap.getUserService().removeByLogin("auto");
    }

    @Test
    public void testAddFindAll() {
        @NotNull final Project projectSecond = new Project();
        projectSecond.setName("AUTO 2");
        bootstrap.getProjectService().add(userId, projectSecond);
        @NotNull final Project projectFirst = new Project();
        projectFirst.setName("AUTO 1");
        bootstrap.getProjectService().add(userId, projectFirst);

        @NotNull List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(2, projectList.size());

        bootstrap.getProjectService().remove(projectFirst);
        bootstrap.getProjectService().remove(projectSecond);
    }

    @Test
    public void testFindRemove() {
        @NotNull Project projectById = new Project();
        projectById.setName("AUTO Id");
        bootstrap.getProjectService().add(userId, projectById);
        @NotNull Project projectByIndex = new Project();
        projectByIndex.setName("AUTO Index");
        bootstrap.getProjectService().add(userId, projectByIndex);
        @NotNull Project projectByName = new Project();
        projectByName.setName("AUTO Name");
        bootstrap.getProjectService().add(userId, projectByName);

        projectById = bootstrap.getProjectService().findById(userId, projectById.getId());
        Assert.assertEquals("AUTO Id", projectById.getName());
        projectByIndex = bootstrap.getProjectService().findByIndex(userId, 1);
        @NotNull List<Project> projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(projectList.get(0).getName(), projectByIndex.getName());
        projectByName = bootstrap.getProjectService().findByName(userId, "AUTO Name");
        Assert.assertEquals("AUTO Name", projectByName.getName());

        bootstrap.getProjectService().removeById(userId, projectById.getId());
        bootstrap.getProjectService().removeByName(userId, "AUTO Name");
        bootstrap.getProjectService().removeByIndex(userId, 1);

        projectList = bootstrap.getProjectService().findAll(userId);
        Assert.assertEquals(0, projectList.size());
    }

}
