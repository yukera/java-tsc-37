package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.constant.FieldConst;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void addAll(@NotNull final Collection<User> collection) {
        for (@NotNull User user : collection) {
            upsert(user);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `user`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `user` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long count() {
        @NotNull final String query = "SELECT COUNT(*) FROM `user`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getLong(1);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final String query = "SELECT * FROM `user`";
        @NotNull final PreparedStatement statement = prepareSql(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        user.setEmail(row.getString(FieldConst.EMAIL));
        user.setFirstName(row.getString(FieldConst.FIRST_NAME));
        user.setLastName(row.getString(FieldConst.LAST_NAME));
        user.setMiddleName(row.getString(FieldConst.MIDDLE_NAME));
        user.setRole(prepare(row.getString(FieldConst.ROLE)));
        user.setLocked(row.getInt(FieldConst.LOCKED) != 0);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query = "SELECT * FROM `user` WHERE `email` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `user` WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new UserNotFoundException();
        return fetch(resultSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT * FROM `user` WHERE `login` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User insert(@NotNull final User user) {
        @NotNull final String query = "INSERT INTO `user` " +
                "(`id`, `login`, `passwordHash`, `email`, `firstName`, `lastName`, `middleName`, `role`, `locked`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getFirstName());
        statement.setString(6, user.getLastName());
        statement.setString(7, user.getMiddleName());
        statement.setString(8, prepare(user.getRole()));
        statement.setInt(9, user.isLocked() ? 1 : 0);
        statement.execute();
        return user;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final User user) {
        @NotNull final String query = "DELETE FROM `user` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, user.getId());
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "DELETE FROM `user` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        @NotNull final String query = "DELETE FROM `user` WHERE `login` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, login);
        statement.execute();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String query = "UPDATE `user` " +
                "SET `login` = ?, `passwordHash` = ?, `email` = ?, `firstName` = ?, `lastName` = ?, `middleName` = ?, " +
                "`role` = ?, `locked` = ? " +
                "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(9, user.getId());
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getFirstName());
        statement.setString(5, user.getLastName());
        statement.setString(6, user.getMiddleName());
        statement.setString(7, prepare(user.getRole()));
        statement.setInt(8, user.isLocked() ? 1 : 0);
        statement.execute();
        return user;
    }

}
