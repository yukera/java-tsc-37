package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public ConnectionService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        Class.forName(serviceLocator.getPropertyService().getJdbcDriver());
        @Nullable final String username = serviceLocator.getPropertyService().getJdbcUsername();
        if (username == null) throw new RuntimeException();
        @Nullable final String password = serviceLocator.getPropertyService().getJdbcPassword();
        if (password == null) throw new RuntimeException();
        @Nullable final String url = serviceLocator.getPropertyService().getJdbcUrl();
        if (url == null) throw new RuntimeException();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
