package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.add(project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Project> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.addAll(collection);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final Project project = new Project();
            project.setName(name);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final Project project = new Project();
            project.setName(name);
            project.setDescription(description);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return changeProjectStatusByName(userId, name, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Project project) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.remove(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.remove(userId, project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            projectRepository.removeByName(userId, name);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findById(userId, id);
        return changeProjectStatus(userId, project, status);
    }

    @NotNull
    @SneakyThrows
    public Project changeProjectStatus(
            @NotNull final String userId,
            @NotNull final Project project,
            @NotNull final Status status
    ) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            project.setStatus(status);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findByIndex(userId, index);
        return changeProjectStatus(userId, project, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return changeProjectStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Project project = findByName(userId, name);
        return changeProjectStatus(userId, project, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findByName(userId, name);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = findById(userId, id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            project.setName(name);
            project.setDescription(description);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findById(userId, id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            project.setName(name);
            project.setDescription(description);
            projectRepository.add(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.findByIndex(userId, index);
        }
    }

}
