package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @NotNull
    Project add(@NotNull String userId, @NotNull Project project);

    @NotNull
    Project findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    void remove(@NotNull String userId, @NotNull Project project);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @NotNull String name);

}
