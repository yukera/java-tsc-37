package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IDataService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.Domain;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class DataService implements IDataService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DataService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;

        serviceLocator.getProjectService().clear();
        @Nullable final List<Project> projects = domain.getProjects();
        if (!DataUtil.isEmpty(projects)) serviceLocator.getProjectService().addAll(projects);

        serviceLocator.getTaskService().clear();
        @Nullable final List<Task> tasks = domain.getTasks();
        if (!DataUtil.isEmpty(tasks)) serviceLocator.getTaskService().addAll(tasks);

        serviceLocator.getUserService().clear();
        @Nullable final List<User> users = domain.getUsers();
        if (!DataUtil.isEmpty(users)) serviceLocator.getUserService().addAll(users);

        serviceLocator.getSessionService().clear();
    }

}
