package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.constant.FieldConst;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    public Project add(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return upsert(project);
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return upsert(project);
    }

    @Override
    public void addAll(@NotNull final Collection<Project> collection) {
        for (@NotNull Project project : collection) {
            upsert(project);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `project`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM `project` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public boolean contains(@Nullable final String id) {
        if (DataUtil.isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `project` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long count() {
        @NotNull final String query = "SELECT COUNT(*) FROM `project`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getLong(1);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final String query = "SELECT * FROM `project`";
        @NotNull final PreparedStatement statement = prepareSql(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setDateStart(row.getDate(FieldConst.DATE_START));
        project.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        project.setStatus(prepareStatus(row.getString(FieldConst.STATUS)));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `project` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `project` WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ProjectNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `project` WHERE `userId` = ? AND `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ProjectNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `project` WHERE `userId` = ? AND `name` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ProjectNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @SneakyThrows
    public Project insert(@NotNull final Project project) {
        @NotNull final String query = "INSERT INTO `project` " +
                "(`id`, `name`, `description`, `dateStart`, `dateFinish`, `userId`, `status`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setDate(4, prepare(project.getDateStart()));
        statement.setDate(5, prepare(project.getDateFinish()));
        statement.setString(6, project.getUserId());
        statement.setString(7, prepareStatus(project.getStatus()));
        statement.execute();
        return project;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Project project) {
        @NotNull final String query = "DELETE FROM `project` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, project.getId());
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        @NotNull final String query = "DELETE FROM `project` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, project.getId());
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "DELETE FROM `project` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "DELETE FROM `project` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final String query = "DELETE FROM `project` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, project.getId());
        statement.execute();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `project` WHERE `userId` = ? LIMIT ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        for (int i = 0; i < index; i++) {
            boolean hasNext = resultSet.next();
            if (!hasNext) throw new ProjectNotFoundException();
        }
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "DELETE FROM `project` WHERE `userId` = ? AND `name` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, name);
        statement.execute();
    }

    @NotNull
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String query = "UPDATE `project` " +
                "SET `userId` = ?, `name` = ?, `description` = ?, `dateStart` = ?, `dateFinish` = ?, `status` = ? " +
                "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(7, project.getId());
        statement.setString(1, project.getUserId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setDate(4, prepare(project.getDateStart()));
        statement.setDate(5, prepare(project.getDateFinish()));
        statement.setString(6, project.getStatus().toString());
        statement.execute();
        return project;
    }

}
