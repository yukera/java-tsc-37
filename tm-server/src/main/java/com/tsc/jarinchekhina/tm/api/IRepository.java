package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E entity);

    void addAll(@NotNull Collection<E> collection);

    @NotNull
    E findById(@NotNull String id);

    void clear();

    void removeById(@NotNull String id);

    void remove(@NotNull E entity);

}
