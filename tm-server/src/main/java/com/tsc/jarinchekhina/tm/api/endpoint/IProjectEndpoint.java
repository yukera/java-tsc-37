package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectEndpoint extends IEndpoint<Project> {

    @NotNull
    Project changeProjectStatusById(@Nullable Session session, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable Session session, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByName(@Nullable Session session, @Nullable String name, @Nullable Status status);

    void clearProjects(@Nullable Session session);

    @NotNull
    Project createProject(@Nullable Session session, @Nullable String name);

    @NotNull
    Project createProjectWithDescription(
            @Nullable Session session,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    List<Project> findAllProjects(@Nullable Session session);

    @NotNull
    Project findProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project findProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project findProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project finishProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project finishProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project finishProjectByName(@Nullable Session session, @Nullable String name);

    void removeProjectById(@Nullable Session session, @Nullable String id);

    void removeProjectByIndex(@Nullable Session session, @Nullable Integer index);

    void removeProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project startProjectById(@Nullable Session session, @Nullable String id);

    @NotNull
    Project startProjectByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    Project startProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project updateProjectById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateProjectByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
