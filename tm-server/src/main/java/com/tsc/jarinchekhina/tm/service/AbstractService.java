package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Override
    public abstract List<E> findAll();

    @NotNull
    @Override
    public abstract E add(@NotNull final E entity);

    @Override
    public abstract void addAll(@Nullable final Collection<E> collection);

    @NotNull
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract void clear();

    @Override
    public abstract void removeById(@NotNull final String id);

    @Override
    public abstract void remove(@NotNull final E entity);

}
