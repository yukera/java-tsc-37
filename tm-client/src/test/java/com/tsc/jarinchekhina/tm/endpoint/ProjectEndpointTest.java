package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Project;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @BeforeClass
    public static void before() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
        Assert.assertNotNull(session);
    }

    @AfterClass
    public static void after() {
        bootstrap.getProjectEndpoint().clearProjects(session);
        @NotNull final Session adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
        bootstrap.getSessionEndpoint().closeSession(adminSession);
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateFindRemove() {
        @Nullable Project projectById = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(projectById);
        @Nullable Project projectByIndex = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        Assert.assertNotNull(projectByIndex);
        @Nullable Project projectByName = bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 3", "Desc");
        Assert.assertNotNull(projectByName);
        @NotNull List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(3,listProjects.size());

        @NotNull String projectId = listProjects.get(0).getId();
        projectById = bootstrap.getProjectEndpoint().findProjectById(session, projectId);
        Assert.assertEquals(listProjects.get(0).getName(), projectById.getName());
        projectByIndex = bootstrap.getProjectEndpoint().findProjectByIndex(session, 2);
        Assert.assertEquals(listProjects.get(1).getName(), projectByIndex.getName());
        projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 3");
        Assert.assertNotNull(projectByName);

        projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 3");
        bootstrap.getProjectEndpoint().removeProjectByIndex(session, 1);
        listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdate() {
        @Nullable Project project = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(project);

        @NotNull String projectId = project.getId();
        project = bootstrap.getProjectEndpoint().updateProjectById(session, projectId, "AUTO","DESC");
        Assert.assertEquals("AUTO", project.getName());
        Assert.assertEquals("DESC", project.getDescription());

        project = bootstrap.getProjectEndpoint().updateProjectByIndex(session, 1, "JUNIT","JDESC");
        Assert.assertEquals("JUNIT", project.getName());
        Assert.assertEquals("JDESC", project.getDescription());

        bootstrap.getProjectEndpoint().removeProjectByIndex(session, 1);
        @NotNull final List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatus() {
        @Nullable Project projectById = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(projectById);
        @Nullable Project projectByIndex = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        Assert.assertNotNull(projectByIndex);
        @Nullable Project projectByName = bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 3", "Desc");
        Assert.assertNotNull(projectByName);

        @NotNull String projectId = projectById.getId();
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().startProjectById(session, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().finishProjectById(session, projectId);
        Assert.assertEquals(Status.COMPLETED, projectById.getStatus());
        projectById = bootstrap.getProjectEndpoint().changeProjectStatusById(session, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().startProjectByIndex(session, 1);
        Assert.assertEquals(Status.IN_PROGRESS, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().finishProjectByIndex(session, 1);
        Assert.assertEquals(Status.COMPLETED, projectByIndex.getStatus());
        projectByIndex = bootstrap.getProjectEndpoint().changeProjectStatusByIndex(session, 1, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByIndex.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().startProjectByName(session, "AUTO Project 3");
        Assert.assertEquals(Status.IN_PROGRESS, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().finishProjectByName(session, "AUTO Project 3");
        Assert.assertEquals(Status.COMPLETED, projectByName.getStatus());
        projectByName = bootstrap.getProjectEndpoint().changeProjectStatusByName(session, "AUTO Project 3", Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

        projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 3");
        bootstrap.getProjectEndpoint().removeProjectByIndex(session, 1);
        @NotNull final List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

}
