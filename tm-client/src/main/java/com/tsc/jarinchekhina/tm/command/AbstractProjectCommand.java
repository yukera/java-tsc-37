package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void print(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().value());
    }

}
